package com.itnove.roman;

import java.util.HashMap;
import java.util.Map;

public class RomanNumeral {

    private Map<String, Integer> table;

    public RomanNumeral() {
        table = new HashMap<String, Integer>();
        table.put("I", 1);
        table.put("V", 5);
        table.put("X", 10);
        table.put("L", 50);
        table.put("C", 100);
        table.put("D", 500);
        table.put("M", 1000);
    }

    public int convert(String roman) {
        return 0;
    }

    private int nextRoman(String roman, int i) {
        return 0;
    }

}
